# MYMONGO.PY

*python commandline tool to write/read MONGODB*

### basic considerations

*It is supposed that two databases are available* `data` and `test`
and the credential file is `~/.mymongo.local`. It should contain json
formated information e.g.:

```
{
"host":"127.0.0.1",
"username":"user",
"password":"xxx",
"authSource":"admin"
}
```

Run `mymongo.py -h`

### Show data from database and collection

`mymongo.py -q` - by default, I expect two databases:  `data` and `test` (also `mysql`).
Each database contains lot of collections (`forecast`, `home_electro` ...)

Each collection contains (it is expected to) time series.

To show/print the data from database (`data`) and collection (`home_electro`):
`./mymongo.py -r data -c home_electro`

### Write data to database and collection

To write data to database `data`, collection `home_electro`:
`./mymongo.py -w data -c home_electro`. If you dont add anything more, only timestamp
is written with all values `NaN`.

Values are **written** with `-v` switch, `key=value` pairs are separated by commas as:
`./mymongo.py -w test -c home_electro -v lowtarif=47274,hightarif=5005`

### Plot data from database and collection

Time series can be plot  with `-p` switch, keys are separated by commas as:
`./mymongo.py -w test -c home_electro -v lowtarif,hightarif`

It is possible to plot only certain **time interval** -
the typical example is the last *28 months*: `-t 28m` :

**Derivatives** - it is possible to plot differentials, it is only necessary to add
**d_** in front of the key: `d_lowtarif,d_heatpump,d_water`.

Derivatives may need a normalization- the differences themselves depend on the time
inteval. An example of the normalization is 1 year: `-D 1y`.


**Two y-axes** plots:  it is possible to use left y-axis for one plot and the right
y-axis for the other. Typical example is the plot of derivatives together with absolute
values. Plot using two lists of parameters after `-p` option.

`./mymongo.py -r data -c home_electro  -D 1y  -t 28m -p  d_lowtarif,d_heatpump,d_water heatpump,hightarif`


**Create jpg** file: The same as with plot - `-p` option and add `-f ~/a.jpg` at the end.
It creates `~/a.jpg`



### Remove data from database and collection







# Installation and user introduction:

## download and install MONGO on ubuntu 16.04


I try newly, after the experiences...
```
aptitude install mongodb mongodb-server mongodb-clients
```

```
systemctl status mongodb
```
show a crash *inspection of /var/log/mongodb/mongo.log shown that it needs 3.5GB for start* editting `/etc/mongodb.conf`

```
dbpath=/var/lib/mongodb -> /home/mongodb
```

While Formely:
```
  aptitude update; aptitude upgrade
  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
  echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
  aptitude update
  aptitude install mongodb-org
```

## running - part 1


Newly:

```
systemctl status  mongodb
mkdir /home/mongodb
chown -R  mongodb:mongodb /home/mongodb
```

While Formely: due to problems with systemctl (maybe an old mongodb  version messing)
```
mkdir -p /data/db
chown -R  mongodb:mongodb /data   # later it will run under this user
###/usr/bin/mongod -v
# RUN UNauthorised
su -s /bin/bash -c  "mongod -v "   mongodb

```





## Install users


```
mongo
use admin
db.createUser(
  {
    user: "admin",
    pwd: "abc123",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
)

```

Now admin is in database admin.  Second **use admin** can be also **use data**, as advised
elsewhere. But _I think_ **use admin** is better.

```
use admin
db.auth("admin", "abc123" )
use admin
db.createUser(
  {
    user: "ojr",
    pwd: "xxx123",
    roles: [ { role: "readWrite", db: "test" },
             { role: "readWrite", db: "data" } ]
  }
)
quit()
```

Now user is in `admin` OR  in `data` and two databases exist `data` and `test`.


## Running authorized

Newly:
```
auth = true
```

I managed to install correctly on stretch,
config `/etc/mongod.conf` contains:
```
storage:
#  dbPath: /var/lib/mongodb
  dbPath: /data/db
  journal:

# network interfaces
net:
  port: 27017
#  bindIp: 127.0.0.1
  bindIp: 0.0.0.0

security:
    authorization: enabled
#    authenticationMechanisms: MONGODB-CR
```

test running with
`mongod -vv  --config /etc/mongod.conf` works,
`service mongod start` fails. Still no reason to
keep this file, unless...




## Entering some test data with shell


```
 use admin
 db.auth("ojr","xxx123")
 use test
 show collections
 db.coltest.insertOne(
 { item:"card", qty:15}
    )
 db.coltest.find()
 show collections
quit()
```



## Mongodump backup

Creates `dump` subdirectory and saves.
```
mongodump -u ojr --db data --authenticationDatabase admin
```


## Management

Download `mongodb-compass-community_1.11.1_amd64.deb`
and install. It doesnt see users, but databases yes.

My tests:
```
use data
while (cursor.hasNext()) { var doc = cursor.next(); if (doc.t isinstanceof ISODate){  db.home_electro.update({"_id": doc._id},{"$set": {"t": new ISODate(doc.t)}} ) }  }

```


## python

`pip3 install pymongo`
`pip3 install pymongo --user` if you like



GIT
-----
Main repo /side:

` git daemon --export-all --base-path=.  `

Slave repo /side - add to config:

`	url = git://gigajm/`

`git pull origin master`









TEST OF TRANSITION
------------------

On fedo: I used to run `/bin/bash -c  "/usr/bin/mongod  --auth"   mongodb`
even from rc.local. All was in
```
drwxr-xr-x   3 mongodb mongodb   4096 úno  3  2018 data
```
/etc/mongod.conf contains, but nothing is in /var/lib/mongodb:
```
# Where and how to store data.
storage:
  dbPath: /var/lib/mongodb
  journal:
    enabled: true
#  engine:
#  mmapv1:
#  wiredTiger:

# where to write logging data.
systemLog:
  destination: syslog
#  logAppend: true
#  path: /var/log/mongodb/mongod.log
#  destination: file
#  logAppend: true
#  path: /var/log/mongodb/mongod.log

# network interfaces
net:
  port: 27017
  bindIp: 127.0.0.1
```

I CHANGED:
```
dbPath: /home/mongodb
```

and run
```
/bin/bash -c  "/usr/bin/mongod  --auth -f /etc/mongod2.conf"   mongodb
```

For the moment it seems to work

# FEW LINES OF SHELL
```
mongo -u ojr -p --authenticationDatabase admin
MongoDB shell version: 3.2.20
Enter password:
connecting to: test
connecting to: test
> show collections
home_electro
telldus
telldus_sensors
>
quit()

```

grant another role:

```
mongo -u admin -p --authenticationDatabase admin          [15:44:40]
MongoDB shell version: 3.2.20
Enter password:
use admin
db.auth("admin","password")
show users
db.grantRolesToUser("ojr",[ {"role":"readWrite","db" :"mysql"} ] )
```

LATEST MONGO:
```
# mongod.conf

# for documentation of all options, see:
#   http://docs.mongodb.org/manual/reference/configuration-options/

# Where and how to store data.
storage:
  dbPath: /home/mongodb
  journal:
    enabled: true
#  engine:
#  mmapv1:
#  wiredTiger:

# where to write logging data.
systemLog:
  destination: syslog

# network interfaces
net:
  port: 27017
  bindIp: 127.0.0.1
```


# Appendix



### sharing this rep[^1]
[^1]: if necessary to transfer got from machine to machine

`git daemon --export-all --base-path=.`

on the other side in .git/config:

`	url = git://gigajm/`




### guerila recovery on ubuntu 1804

- copy /home/mongodb from backup
- edit `emacs /etc/mongodb.conf`
- ` systemctl restart mongodb`
- create  `~/.mymongo.local`
