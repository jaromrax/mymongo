#!/usr/bin/python3
import pymysql.cursors
import pymysql    # recommended over MySQLdb

import mymongo
################################################
#
# THIS IS CONVERTSION OF MYSQL TO  MONGODB
#   2 old mysql databases on fedo
#   to (what is default) on mymongo.py/pymongo/
#
#
###############################################
# Connect to the database
mysqlConnection = pymysql.connect(host='localhost',
                             user='ojr',
                             password='xxx',
                             db='test',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
#############################################
#   COLLECTION
############################################
db=mymongo.get_mongo_database( 'test' )
COLLECTION='home_electro'
collection=db[ COLLECTION ]
try:
    with mysqlConnection.cursor() as cursor:
        sql = "SELECT * FROM `elektromery` "
        cursor.execute(sql)
        #result = cursor.fetchone()
        result = cursor.fetchall()
        for i in result:
            print(i)
            i2={ "lowtarif":  i['b'],
                 "hightarif": i['a'],
                 "heatpump":  i['c'],
                 "water":     i['d'],
                 "t": i['t']
            }
            #val=mymongo.prepare_write_no_time( i2 )
            #result=collection.insert_one( val ) # INSERT HERE
        #print( len(result),"results" )
        #print(result)
finally:
    print()
quit()
#################################################################
#############################################
#   COLLECTION
############################################
db=mymongo.get_mongo_database( 'test' )
COLLECTION='home_sensors'
collection=db[ COLLECTION ]
try:
    with mysqlConnection.cursor() as cursor:
        sql = "SELECT * FROM `db` "
        cursor.execute(sql)
        #result = cursor.fetchone()
        result = cursor.fetchall()
        for i in result:
            print(i)
            i2={ "out_t"  :  i['a'],
                 "out_hum":  i['b'],
                 "kuch_t" :  i['c'],
                 "kuch_hum": i['d'],
                 "koup_t" :  i['e'],
                 "koup_hum": i['f'],
                 "cerp_t" :  i['g'],
                 "puda_t":   i['h'],
                 "t": i['t']
            }
            val=mymongo.prepare_write_no_time( i2 )
            #result=collection.insert_one( val ) # INSERT HERE

        #print(result)
        #print( len(result),"results" )
finally:
    mysqlConnection.close()





# import  MySQLdb
# db = MySQLdb.connect(host="localhost",    # your host, usually localhost
#                      user="ojr",         # your username
#                      passwd="xxx",  # your password
#                      db="test")        # name of the data base
# # you must create a Cursor object. It will let
# #  you execute all the queries you need
# cur = db.cursor()

# # Use all the SQL you like
# cur.execute("SELECT * FROM elektromery")

# # print all the first cell of all the rows
# res=cur.fetchall()
# print( len(res) )
# for row in res:
#     print(row)

# db.close()
