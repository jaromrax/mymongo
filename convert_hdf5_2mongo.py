#!/usr/bin/python3
import mymongo
import datetime
###################################
#
#  i have lost MONGO
#     hdf5 remained me
#
#  also i can create hd5 and later use this
#
####################################
import time
import progressbar  # pip3 install progressbar2

import argparse

from pandas import read_hdf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#===================================
parser=argparse.ArgumentParser(description="",
usage="""
 -i hd5file  -w database  -c collection
""",  formatter_class=argparse.RawTextHelpFormatter    )

parser.add_argument('-d','--debug', action='store_true' , help='')
parser.add_argument('-i','--input',default="", help="input hdf5 file")
parser.add_argument('-k','--key', type=int, default=0, help="input key in  hdf5 file")
parser.add_argument('-w','--write',default="", help="write database test|data")
parser.add_argument('-c','--collection', default="", help='collection name  home_temp')
args=parser.parse_args()
if args.input=="":
    print("X...  no input hdf5 given")
    quit()
if args.debug:
    print("D...  debugging")

print("i...  ========================= hdf5  part =============")

hdf5name=args.input

with pd.HDFStore(hdf5name) as hdf:
    print("i... keys         :",hdf.keys() )
    keyname=hdf.keys()[  args.key ]
    print("i... selected key :",  keyname )
df=pd.read_hdf( hdf5name, keyname )
#df_reader = hdf.select('my_table_id', chunksize=10000)
#df = pd.read_hdf( hdf5name, 'd1' ) #,
               #where=['A>.5'], columns=['A','B'])
#plt.plot( df['t'] , df['hightarif'] ,'.')
           
print("i... DFrame ok;len:",len(df))
print("i... columns      :", list(df.columns.values) )
df = df.sort_values(by=['t'], ascending=0)

#quit()

#############################################
#   COLLECTION 
############################################
print("i...  ========================= mongo part =============")
if args.write=="":
    print("X... database not given  -w")
    quit()
db=mymongo.get_mongo_database( args.write,"~/.mymongo.local",silent=False)
print("i... database:",db)
if args.collection=="":
    print("X... destination collection not given  -c")
    quit()
COLLECTION='hdf5imported'
COLLECTION=args.collection
collection=db[ COLLECTION ]


#######################
#
#   loop over  df
#      and prepare DICT to enter in mongo
#######################
names=list(df.columns.values)
#
print("i... .........iterations......")
total=len(df)
cur=0
##############################################
#for i in progressbar.progressbar(range(100)):
#    time.sleep(0.02)
with progressbar.ProgressBar(max_value=total) as bar:
    #print( "{:9d}/{:9d}".format(total,total) , end="\n" )
    for i,row in df.iterrows():  # going one by one
        cur+=1
        bar.update(cur)
        #if cur%100==0:
        #    print( "{:9d}/{:9d}".format(cur,total) , end="\r" )
        ins={}
    
        for j in names:
            #print( "{:7d} {:20s} {}".format( i, j , row[j]  ) )
            if j=='t':
                ttt=pd.to_datetime( row[j] )             #print(row[j], ttt)
                row[j]=ttt.strftime('%Y-%m-%d %H:%M:%S') #STR needed
                #print( row[j] , type(row[j]) )
            ins[j]=row[j]
        #print( "{:6d}".format(i), ins,"\n" )
    
    #    ins={
    #        "t": row['t'],
    #        "lowtarif":  row['lowtarif'],
    #        "hightarif": row['hightarif'],
    #        "heatpump":  row['heatpump'],
    #        "water":     row['water'],
    #    }
        #print( ins )
        val=mymongo.prepare_write_no_time( ins ) #prep:not t
        #print(val)
        result=collection.insert_one( val ) # INSERT HERE
    ####print( len(result),"results" )
    #print(result)
    
