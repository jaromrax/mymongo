#!/usr/bin/python3
##################################
#   LEVEL 2 .
#   program + module
#   1. outputs dataframe
#   2. can trig plotting
#   3. can output to hdf5 ?
#
#
################################################
#   this should be the best what i can 2018/1
#    -  timeout
#    -  if non-connection => quit
#    -  difference between test and data (2 musthave dbases)
#    -  json - advanced columns
#      ~/.mymongo.local    credentials
##################################


from pymongo import MongoClient
import pymongo # to get ASCENDING
import datetime
import time

#
# /home/milous/.local/lib/python3.6/site-packages/pandas/plotting/_matplotlib/converter.py:103: FutureWarning: Using an implicitly registered datetime converter for a matplotlib plotting method. The converter was registered by pandas on import. Future versions of pandas will require you to explicitly register matplotlib converters.
#
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

#  from monary import Monary #  10x faster BUT CANNOT FIND mongoc.h
import numpy
import numpy as np
import pandas as pd

import argparse
import os
import json

import matplotlib.pyplot as plt
import matplotlib.dates as mdates

DEBUG=0  # will change in MAIN  args





#
##### trick to profile functions
#
def timing(f):
    def wrap(*args,**kwargs):
        time1 = time.time()
        ret = f(*args,**kwargs)
        time2 = time.time()
        if DEBUG:print('### {:s} function took {:.3f} ms'.format(f.__name__, (time2-time1)*1000.0))

        return ret
    return wrap

############################################
#  MONGODB  FUNCTIONS
############################################
@timing
def read_mongo_credentials( collection , silent=False):
    """
      real filename with credentials  ~/.mymongo.local
    """
    cfgfile=os.path.expanduser(collection)
    if DEBUG:print("D... reading credentials", cfgfile)
    if not silent:print("i... reading ", cfgfile , end="" )
    cdict={}
    if os.path.isfile( cfgfile ):
        with open( cfgfile ,'r') as f:
            cdict=json.load( f )
        if not silent:print( ' ... HOST=',cdict['host'] )
    else:
        if not silent:print(" !... NO FILE FOUND")
        if not silent:print("""
      try to create something like:
{
"host":"127.0.0.1",
"username":"user",
"password":"xxx",
"authSource":"admin"
}
        """)
        quit()
    return cdict







# from reading file, gives an extra question, returns client
#   ~/.mymongo.local   assumed   TEST and DATA
@timing
def get_mongo_database( database , initialconfig , silent=False  ):
    if database=="":
        print("X... no database given")
        return None
    #print(args)
    #dic=read_mongo_credentials( args.initialconfig ,silent=silent)
    dic=read_mongo_credentials( initialconfig ,silent=silent)
    #print(dic)
    if DEBUG:print("D... has read credentials")
    if DEBUG: print( "D... ####",dic )
    if dic['host']=="":
        print("D.... host not defined incfg") # rare case
        quit()
    client = MongoClient( dic['host'],
                     username=dic['username'],
                     password=dic['password'],
                     authSource=dic['authSource'],
#                    authMechanism='SCRAM-SHA-1', # !!! later versions
                     serverSelectionTimeoutMS=1000,
                     connect=False
     )
   #  { "host":"127.0.0.1",
   # "username":"ojr",
   # "password":"aaaa",
   # "authSource":"data"
   #     }

    ###---- ONE EXTRA question == slowdown, but no messing later
    q=False
    if DEBUG:print("D... extra slowingdown question on ismaster")
    try:
        if DEBUG:print("D... asking admin: ismaster: ... ")
        result = client.admin.command("ismaster")
        if DEBUG:print("D... client is OK")
    except:
    #except errors.ConnectionFailure:
        print("!... Connection failed <- ismaster check")
        q=True
    if q:
        try:
             errors
        except:
            print("!... `errors` object not found ... mongo database doesnt exist?")
            print("!...  may authSource is not 'auth' but 'data' ? ")
            print("!... quitting");quit()
    #not authorizes!!!  print( client.database_names() )
    #print(    client.database_names() )

    return client[ database ]







@timing
def tell_mongo_collections( database , initialconfig ):
    """
    lists collections in the database  database/test/data/...
    """
    dbase=get_mongo_database( database ,initialconfig, silent=True)
    if DEBUG:print("i... dbase obtained. Searching collections names:", database )
    # collection = dbase.collection_names(include_system_collections=False) # DEPREC
    collection = dbase.list_collection_names(include_system_collections=False)
    #print(collection)

    #print( "{:10s}   {}".format( database,sorted(collection) ) )
    print( "\n=== {:10s} ==========================".format( database ) )
    print(   " collection name      | number of records".format( database ) )
    print(   "_________________________________________" )

    #cursor = dbase[args.collection].find({})
    #for document in cursor:
    #    print(document)
    for i in sorted(collection):
        print("{:>19s}  ".format(i) , end="" )
        a=[]
        ####a=dbase[ i ].aggregate([
        ##doc=dbase[ i ].findOne()
        ##for k in doc: print(k)
        # a=dbase[ i ].aggregate([
        #     {"$project":{"arrayofkeyvalue":{"$objectToArray":"$$ROOT"}}},
        #     {"$unwind" :"$arrayofkeyvalue"},
        #     {"$group":{"_id":None,"allkeys":{"$addToSet":"$arrayofkeyvalue.k"}}}
        # ])
        # la=dbase[ i ].count() # DEPREC
        la=dbase[ i ].estimated_document_count()

        #la=list(a)
        print("   {:9d}".format(  la )   )

    return collection





#  create DICT with floats from programmed arguments
#def prepare_write( **kwargs ):  # a=1,b=2....
@timing
def prepare_write_no_time( kwargs , silent=True):      # DICT
    tnew=datetime.datetime.strptime(kwargs['t'],"%Y-%m-%d %H:%M:%S")
    val={"t":tnew }
    for k in kwargs.keys():
        if not silent:print("    k:w=",k,":",kwargs[k] )
        if kwargs[k] is None:
            val[ k ] = None
        elif k!="t":
            val[ k ] = float( kwargs[k] )

    if not silent:print("FINAL",val)
    return val


######   NORMAL -  WITH AUTOMATIC TIME ########
@timing
def prepare_write( kwargs ):      # DICT
    val={ "t":datetime.datetime.now()
    }
    for k in kwargs.keys():
        print("    k:w=",k,":",kwargs[k] )
        if kwargs[k] is None:
            val[ k ] = None
        else:
            val[ k ] = float( kwargs[k] )
    print("FINAL",val)
    return val



@timing
def get_date_from_mark( atime ,string=False ): # 1d 1h 1w
    """ enter  string number+time_unit like 5m 30m 1h 3d 5w
        returns START time
              Either  timedelta
              Or      string (if mongodb t is string)
    """
    unit=1
    if atime[-1]=='S': unit=1
    if atime[-1]=='M': unit=60
    if atime[-1]=='H': unit=60*60
    if atime[-1]=='d': unit=60*60*24
    if atime[-1]=='m': unit=60*60*24*30
    if atime[-1]=='y': unit=3600*24*365
    if atime[-1]=='w': unit=60*60*24*7
    val=atime[:-1]
    ret=float(val) * unit
    print( "D... time distance:  value=",val," unit=", unit ," ... seconds=", ret )
    start=datetime.datetime.now() - datetime.timedelta( seconds= ret)
    print("D... fetching from DATE: ",start)
    if string:
        start=start.strftime("%Y-%m-%d %H:%M:%S")
    return start






##########################################################
#           COMPLETE      ACTIONS IN FUNCTIONS
#
##########################################################
@timing
def action_query():
    print("i... I am trying to list databases: data, test, mysql")
    try:
        tell_mongo_collections( 'data' ,args.initialconfig )
    except:
        print("X... data database ... not seen")
    try:
        tell_mongo_collections( 'test' ,args.initialconfig )
    except:
        print("X... test database ... not seen")
    try:
        tell_mongo_collections( 'mysql' ,args.initialconfig )
    except:
        print("X... mysql database ... not seen")


@timing
def action_read( DATABASE, COLLECTION ):
    if DEBUG:print("i... reading DATABASE=",DATABASE,";   COLLECTION=",COLLECTION)



##########################################################
#           experimental functions - integrate pandas
#
#########################################################
#https://stackoverflow.com/questions/43900677/definite-numerical-integration-in-a-python-pandas-dataframe
#https://stackoverflow.com/questions/26456825/convert-timedelta64ns-column-to-seconds-in-python-pandas-dataframe
import numpy as np
@timing
def integratepd(x, y):
    """
    x : pandas.Series
    y : pandas.DataFrame
    """
    mean_y = (y[:-1] + y.shift(-1)[:-1]) / 2
    # these deltas are negative ! but time goeas backwards
    delta_x = (x.shift(-1)[:-1] - x[:-1] )/ np.timedelta64(1, 'h')
    #delta_x = (x[:-1] - x.shift(-1)[:-1] )/ np.timedelta64(1, 'h')
    #print( type(delta_x))
    scaled_int = mean_y.multiply(delta_x)
    #print( ">> ",delta_x ," MEAN Y>>",  mean_y ,"scaled INT", scaled_int)
    cumulative_int = scaled_int.cumsum(axis='index')
    res=cumulative_int.shift(1).fillna(0)
    return res-res.iloc[-1]





################################################################################
#       MAIN
################################################################################
if __name__ == "__main__":
    #print("MAIN")
    ####################################
    # PARSER ARG
    ######################################
    #parser = argparse.ArgumentParser(description='details',
    parser=argparse.ArgumentParser(description="",
        usage="""use "%(prog)s --help" for more information

MONGO has database and collection;
MYMONGO records floats with a current time OR reads this data
------------------------------------------------------------------
USAGE:
------------------------------------------------------------------
        -r/-w specifies database; -c spec. collection
# READ
-q  ... get collection names /i need credentials!!!/
-r data -c coll          ... show last 999999 values
-r data -c coll  -n 10   ... 10 last values
        -r data -c coll  -t 1d  ... show last 1day
             -t [ HMSymd w] ( M-inutes ,S-ecs, H-ours, m-onth... )

# WRITE
VALUES TO INSERT:
-w data -c mycollection
   -v lowtarif=33930,hightarif=3961,heatpump=11443,water=3549

VALUES TO INSERT, cheating with time:
   -v "t=2018-01-31 11:22:33,lowtarif=33930,hightarif=3961,heatpump=11443,water=3549"

eXXXtra function - dangerous: ______________________________________
mymongo.py -r test  -c sors  -x find/'{"koupelna_th":{"$gte":35.1}}'
mymongo.py -r test  -c sors  -x remove/'{"koupelna_th":{"$gte":35.1}}'

mymongo.py -r test  -c sors  -x find/'{"kuchyne_th":{"$lt":-10}}'
mymongo.py -r test  -c sors  -x remove/'{"kuchyne_th":{"$lt":-10}}'

mymongo.py -r data -c timed  -x remove/'{"high":{"$exists":false}}'


mymongo.py -r test --removecollection rains   #  DROP THE COLLECTION
____________________________________________________________________

        # PLOT  FIELDS:
mymongo.py -r data -c home_electro -p water,hightarif,lowtarif,heatpump
        # PLOT  FIELDS 2 y-AXES:
mymongo.py -r data -c home_electro -p water,hightarif,lowtarif heatpump
        # PLOT  FIELDS WITH INTEGRAL (rains ... mm/h => total mm):
mymongo.py -r data -c rains -t 1d -pi cr
        # PLOT  FIELDS WITH DERIVATIVE (electro consuption kWh => rate*Norm):
mymongo.py -r data -c home_electro -t 1y -D 1y -p d_heatpump,d_lowtarif

        # SAVE FIGURE:
mymongo.py -r data -c home_electro -p water -f
        # SAVE DATA:
mymongo.py -r data -c home_electro -p water -s datafile.h5
        # PRINT THE LAST non null VALUE  ... val extraction in a script
mymongo.py -r test -c telldus_sensors -n 1 -v koupelna_t | tail -1
------------------------------------------------------------------

""",  formatter_class=argparse.RawTextHelpFormatter    )


    parser.add_argument('-d','--debug', action='store_true' , help='')

    parser.add_argument('-r','--read', default="", help="read  database test|data")
    parser.add_argument('-w','--write',default="", help="write database test|data")
    parser.add_argument('-c','--collection', default="", help='collection name  home_temp')

    parser.add_argument('--removecollection', default="", help='remove collection: like home_temp')

    parser.add_argument('-x','--xxx',default="", help="special command with -w  -c")

    parser.add_argument('-q','--querycollections',action="store_true" )
    parser.add_argument('-v','--values',default="", help="",nargs="+")

    parser.add_argument('-n','--n',type=int,default=9999999,  help="")
    parser.add_argument('-t','--time',default='',  help="time ago:  5m, 1h, 2d, 3w, 1y")


    parser.add_argument('-s','--hdf5',default="", help="hdf5 file to store data" )

    parser.add_argument('-f','--figure',default="", help="filename to save the numbers (.txt) OR  plot ( -p necessary)")
    parser.add_argument('-p','--plot',default="", help="",nargs="+") # LAST ARGUMET !!!!!!!!
    parser.add_argument('-b','--background',default="white", help="background color of a plot") #
    parser.add_argument('-o','--points_off',action="store_true", help="dont use point in plot")

    parser.add_argument('-I','--INTEGRAL',action="store_true" )
    parser.add_argument('-D','--DERIVATIVE',default="" , help="time base:  1s 1h 1d 1m 1y" )

    #parser.add_argument('service', action="store", nargs="+")
    #parser.add_argument('command', action="store")




    parser.add_argument('-i','--initialconfig', default="~/.mymongo.local", help="~/.mymongo.local")

    #parser.add_argument('service', action="store", nargs="+")
    #parser.add_argument('command', action="store")
    args=parser.parse_args()
    #print( args )


    #################################################### ARG END
    #################################################### ARG END
    #################################################### ARG END

    if args.debug:
        DEBUG=1
        print("D...   DEBUG ON")
    else:
        DEBUG=0

    # DECIDE    READ OR WRITE =====================
    # Set the db object to point to the data database
    READ=False
    if args.write=="": READ=True
    if READ:
        DATABASE=args.read
    else:
        DATABASE=args.write





    ##################################### IF -q ########
    if args.querycollections:
        action_query()

        quit()

    # duplicate -------------------------- later there is a drop also
    if args.removecollection!="":
        action_query()
        print("REMOVING COLLECTION ",args.removecollection," FROM DATABASE",DATABASE)
        key=input('??? Are you sure to remove (y/n) ???')      # If you use Python 3
        if key!="y":
            print("just quited.")
            quit()
        key=input('??? Are you sure to remove (y/n) ???')      # If you use Python 3
        if key!="y":
            print("just quited.")
            quit()
        print("i... DATABASE=/"+DATABASE+"/" )
        db=get_mongo_database( DATABASE , args.initialconfig)
        db.drop_collection( args.removecollection )
        action_query()
        quit()



    # prepare insert values ---------------- make THEM DICT
    insdict={}
    if not READ and len(args.values)>0:
        if DEBUG: print("D...   preapring write - general")
        insertvals=",".join(args.values)
        insertvals=insertvals.split(",")
        for i in insertvals:
            key=i.split("=")[0]
            val=i.split("=")[1]
            insdict[key]=val
        if DEBUG:print("D... INSERT DICT:", insdict )


        print("DATABASE  | COLLECTION LIST")
        print("--------  | --------------")

    #if READ and len(args.values)>0: ###  READ ONLY SPECIFIC DOCUMENTS
    #    print("i...   NEW FEATURE - READ ONLY non null ", args.values)

    print("i... DATABASE=/"+DATABASE+"/" )
    db=get_mongo_database( DATABASE , args.initialconfig )
    if DEBUG:print("D... got database ", db)
    COLLECTION=args.collection
    print("i... COLLECTION=/"+ COLLECTION+"/")
    q=False
    if COLLECTION != "":
        try:
            collection=db[ COLLECTION ]
            if DEBUG:print("D... got collection", collection)
        except:
            print("!... no collection named /"+COLLECTION+"/ in database /"+DATABASE+"/")
            q=True
    else:
        q=True
    if q:print("!... no collection named /"+COLLECTION+"/ in database /"+DATABASE+"/"); quit()




    if args.xxx!="":
        print("X... !!!   eXXXperimental COMMAND  !!!   function/argument")
        timequest={}
        if args.time!="":   # AS EARLIER
            start=get_date_from_mark( args.time, string=False ) # 1d 1h 1w
            timequest={"t": {"$gte": start}}
            print('                ',timequest)
            print('                ',timequest['t'])
            # this doesnt work in t:Date
            #   find( {'t': {'$gte': start}} ).sort([('t',-1)])

#         print( """
# EXAMPLES:
# mymongo.py -r test  -c telldus_sensors  -x find/'{"koupelna_th":{"$gte":35.1}}'
# mymongo.py -r test  -c telldus_sensors  -x remove/'{"koupelna_th":{"$gte":35.1}}'

# mymongo.py -r test  -c telldus_sensors  -x find/'{"kuchyne_th":{"$lt":-10}}'
# mymongo.py -r test  -c telldus_sensors  -x remove/'{"kuchyne_th":{"$lt":-10}}'
# """)
        if len(args.xxx.split("/"))<2: quit()
        function=args.xxx.split("/")[0]
        argument=args.xxx.split("/")[1]
        print("function =  ", function," ======================" )
        print("argument =  ", argument )
        #
        # must move from single quote to double
        json_acceptable_string = argument.replace("'", "\"")
        d = json.loads( json_acceptable_string )
        print("dict json : ",d)
        if 't' in timequest:
            d['t']=timequest['t']  # I WILL ADD THE TIME LIMITS
            print("dict json2: ",d)
        #d={"kuchyne_th": {"$gte": 1}}
        #print("dict fixed: ",d, d["kuchyne_th"])
        ###collection.ensure_index([("t", pymongo.ASCENDING)])
        print("--------------------------------ACTIONS FROM NOW-----")
        if (function=="find") or (function=="remove"):
            cursor=collection.find( d , {'_id': False}  ) # cursor is ephemerous! convert once to list
            li=list(cursor)
            print('=begin=================================== results:',len(li) )
            for i in li:  print( i )
            print('=end  =================================== results:',len(li) )

        if function=="remove":
            print("DATABASE",args.write,args.read, "COLLECTION",args.collection )
            print(d)
            key=input('??? Are you sure to remove (y/n) ???')      # If you use Python 3
            if key!="y":
                print("quit.")
                quit()
            if argument=="{}":
                print("are you SURE? - ALL ?   5 seconds to ctrl c")
                time.sleep(5)
            cursor=collection.remove( d  ) # cursor is ephemerous! convert once to list
            li=list(cursor)
            print('results:', li)
        if function=="drop":
            print("DATABASE",args.write,args.read, "COLLECTION",args.collection )
            key=input('??? Are you sure to remove (y/n) ???')      # If you use Python 3
            if key!="y":
                print("quit.")
                quit()
            if argument=="{}":
                print("are you SURE? - ALL ?   5 seconds to ctrl c")
                time.sleep(5)
            collection.drop( )  # DROP COLLECTION ???
        print("\n!... quitting eXXXperimental featu before making more damage")
        quit()
    #collection.remove( {"koupelna_th_dew":{"$gt":50} }  )









    ###################### READ SECTION ##################
    if READ:
        #
        action_read(  DATABASE, COLLECTION )
        if DEBUG:print("i... reading DATABASE=",DATABASE,";   COLLECTION=",COLLECTION)
        # SORT BEFORE LIMIT !     t -1 == latest
        # index will avoid 32MB limit...............
        #  i should ENSURE index with t
        # deprec....collection.ensure_index([("t", pymongo.ASCENDING)])
        collection.create_index([("t", pymongo.ASCENDING)])
        # ----
        READ_CONDITION={}
        if args.time!="":
            #  get STRING OR  Timedelta?  homne_elec  TIMEDELTA!
            start=get_date_from_mark( args.time, string=False ) # 1d 1h 1w
            #print(start)#just repeats
            #READ_CONDITION={'t': {'$gte': start}}
            READ_CONDITION['t']={'$gte': start}
            print("D... condition for t: ", READ_CONDITION['t'])
        for ii in args.values:
            if DEBUG:print("D... ARGS.VALUES : ",  ii, args.values)
            #READ_CONDITION[ args.values ]={'$exists':"true" }
            READ_CONDITION[ ii ]={'$ne':None }
        # if args.time=="":
        #     if DEBUG:print("D... limit n last")
        #     cursor=collection.find( READ_CONDITION ).sort([('t',-1)]).limit( args.n )
        #     #cursor=collection.find()
        # else:
        #     #  get STRING OR  Timedelta?  homne_elec  TIMEDELTA!
        #     start=get_date_from_mark( args.time, string=False ) # 1d 1h 1w
        #     print(start)
        #     READ_CONDITION={'t': {'$gte': start}}
        #     # this doesnt work in t:Date
        if DEBUG:print("D... READ_CONDITION:",READ_CONDITION)
        cursor=collection.find(     READ_CONDITION  ).sort([('t',-1)]).limit( args.n )
        #cursor=collection.find(     READ_CONDITION  ).sort([('t',-1)])
        #cursor=collection.find(  {'t':{'$lt': datetime.datetime(2018, 3, 10, 1, 21, 48 )}}     ).sort([('t',-1)])
        #cursor=collection.find(    ).sort([('t',-1)])


        # -----
        licursor=list(cursor)
        if DEBUG: print("D... cursor obtained ... len==", len(licursor) )
        print("i... ", len(licursor),"elements from collection",COLLECTION )
        if len(licursor)==0:
            quit()
        df=pd.DataFrame( licursor )
        del df["_id"]               # DELETE _id, dont mess with it
        #  nicer display: make 't'  be the 1st column ==================
        cols = df.columns.tolist()
        if 't' in cols:
            while cols[0]!='t':
                cols=cols[-1:]+cols[:-1]
        else:
            if DEBUG:print("D...  NO TIME variable.... 't' expected")
        df=df[cols]
        ##############################
        #---------- PRINTOUT - FINAL ===========================================
        #
        print( df )
        #       print full table:
        #            with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        #                print(df)
        #  WRITE TXT -f a.txt
        if ( len(args.figure)>0) and( args.figure.find(".txt")>=len(args.figure)-5 ):
            # TRICK TO PRINT TO txt FILE:
            #df.to_csv(args.figure, sep="\t", index=False)
            with pd.option_context('display.max_rows', None, 'display.max_columns', None):
                with open( args.figure,"w") as f:
                    f.write('''---
documentclass: extarticle
fontsize: 8pt
geometry: [top=0.5cm, bottom=0.5cm, left=1.5cm, right=1cm]
---
''')
                    outlines=df.__str__().split("\n")
                    for i in outlines:
                        f.write( "    "+i+"\n" ) # I NEED MAGIC OF 4 spaces and after
                        #      -f markdown+hard_line_breaks is not necessary, courier works...
                    print("D... table in file", args.figure)
                    print("        pandoc    ",args.figure,"  -o o.pdf")

        #
        #############################

        for ii in  args.values:     # PRINT THE LAST VALUE of the column
            print( df[ii].iloc[0] ) # 1st in dataframe by time

        if args.DERIVATIVE != "":
            factor=0
            # correspodning value over time period
            multiple=(args.DERIVATIVE[:-1]) # should be 1
            print("D...  multiple=", multiple)
            multiple=float( multiple ) # should be 1
            if args.DERIVATIVE[-1]=="S": factor=1
            if args.DERIVATIVE[-1]=="M": factor=60
            if args.DERIVATIVE[-1]=="H": factor=3600
            if args.DERIVATIVE[-1]=="d": factor=3600*24
            if args.DERIVATIVE[-1]=="m": factor=3600*24*30
            if args.DERIVATIVE[-1]=="y": factor=3600*24*365
            if args.DERIVATIVE[-1]=="w": factor=3600*24*7
            print("D...  derivating all now ......................",factor,multiple)
            factor=factor*multiple
            for i in cols: #  there is t here!!!!
                #df['d_'+i]=df[i].diff()/df['t'].diff().dt.total_seconds()*factor
                df['d_'+i]=-df[i].diff( periods=-1 ) # create DIFF
            for i in cols: #  there is t here!!!!
                if i!='t':                           # MAKE RATE norm on TIME & FACTOR
                    df['d_'+i]=df['d_'+i]/df['d_t'].dt.total_seconds()*factor
            df['d_t']=-df['t'].diff( periods=-1 )
            df['t2']=df['t']-df['t'].diff( periods=-1 )/2 # in the midle of period


            print(df)
        ###########################   PLOT  ##############
        #  idea:  t1,t2,t3 h1,h2,h3   would make two axes best
        #
        #
        if args.plot:
            if DEBUG:print( args.plot )
            #================ prepare two axes lists

            # NEW TST 2 axes===========
            PLOLIST1=[]
            PLOLIST2=[]
            if len(args.plot)==1:
                PLOLIST1=args.plot[0].split(",")
            if len(args.plot)==2:
                PLOLIST1=args.plot[0].split(",")
                PLOLIST2=args.plot[1].split(",")
            print( PLOLIST1 )
            print( PLOLIST2 )
            fig=plt.figure( )
            fig.patch.set_facecolor( args.background )
            #fig=plt.figure(  figsize=(5, 4), dpi=100 )
            host = fig.add_subplot(111)
            numero=0  # COUNTER yax1 yax2
            markcols=['b.-','r.-','g.-','c.-','m.-','y.-', 'tab:orange',  'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive']
            if args.points_off:
                markcols=['b-','r-','g-','c-','m-','y-', 'tab:orange',  'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive']
            markcol2=['bx','rx','gx','cx','mx','yx', 'tab:orange',  'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive']
            plot_lns=[]   # legend is organized here
            ###########
            # axis 1
            #
            # here is INTEGRAL, so i put DERIVATE also? ------ but DRV should be in pandas...
            ###########
            restlabels=[] # y2 will have al the labels on yaxis
            for i in PLOLIST1[:len(markcols)-2]:
            #for i in args.plot[:len(markcols)-2]:
                if numero>=0:
                    logic_array = ~(np.isnan( df[i] )) # BIG TRICK - void NaN
                    # I plot normally OR derivative "t2"
                    if args.DERIVATIVE:
                        # I transform from np to LISTS: bar cannot plot np well.
                        # binb width from d_t * 0.9; colors game
                        print("D... axis 1; derivative done in pandas; drawing vs. 't2'")
                        aaa=[ x for x in df.t2[logic_array]  ]
                        daa=[ -x.total_seconds()/24/3600*0.9 for x in df.d_t[logic_array]  ]
                        bbb=[ int(x) for x  in  df[i][logic_array] ]
                        #print(  aaa )
                        #print( daa )
                        #print( bbb )
                        p1=host.bar( aaa , bbb , width=daa  , label=i ,edgecolor=markcol2[numero][0] ,color=markcol2[numero][0] ,alpha=0.2)
                        p1,=host.plot( df.t2[logic_array] , df[i][logic_array] , markcol2[numero] , label=i)

                    else:
                        p1,=host.plot( df.t[logic_array] , df[i][logic_array] , markcols[numero] , label=i)

                    if args.INTEGRAL:
                        print("D...  ploting integral axis 1")
                        ## EXPERIMENT  INTEGRATE
                        Ingral=integratepd( df.t[logic_array] , df[i][logic_array]   )
                        print( 'Integral ==',Ingral.iloc[-1]," to ",Ingral.iloc[0])
                        p1a,=host.plot( df.t[logic_array][:-1] , Ingral , markcols[numero][0]+':' )
                        ## EXPERIMENT  INTEGRATE

                    ####p1,=host.bar( df.t , df[i]  )
                    restlabels.append( i )
                    plot_lns.append( p1 )
                    host.set_ylabel(i)
                    #
                    preleg=""
                    if len(args.DERIVATIVE)>0:
                        preleg="[ Norm="+args.DERIVATIVE+"]  "
                    host.set_ylabel( preleg +";".join(restlabels)[:50]  ) #, color=markcols[numero][0]
                numero=numero+1

            plt.gca().grid() # I put grid here to be aligned with y-values (left)
            ###########
            # axis 2
            ###########
            restlabels=[] # y2 will have al the labels on yaxis
            numero2=0
            for i in PLOLIST2[:len(markcols)-2]:
            #for i in args.plot[:len(markcols)-2]:
                if numero2==0:
                     ax2=host.twinx()
                     logic_array = ~(np.isnan( df[i] ))
                     p1,=ax2.plot( df.t[logic_array] , df[i][logic_array] , markcols[numero] , label=i)
                     if args.INTEGRAL:
                        print("D...  axis 2 CANNOT HAVE INTEGRAL !")

                     restlabels.append( i )
                     ax2.set_ylabel(  i , color=markcols[numero][0] )
                     print("D... ValueForAxis: set_ylabel   color=",markcols[numero][0])
#                     ax2.tick_params( i , colors=markcols[numero][0] )
#                     print("D... ValueForAxis: tick_params   color=",markcols[numero][0])
                     # aligning right yticks with left side
                     #
                     #ax2.set_yticks(np.linspace(ax2.get_yticks()[0], ax2.get_yticks()[-1], len(host.get_yticks()) ) )

                     plot_lns.append( p1 )
                if numero2>=1:
                     logic_array = ~(np.isnan( df[i] ))
                     p1,=ax2.plot( df.t[logic_array] , df[i][logic_array] , markcols[numero] , label=i)
                     restlabels.append( i )
                     ax2.set_ylabel(  ";".join(restlabels)[:50]  ) #, color=markcols[numero][0]
                     plot_lns.append( p1 )
                numero=numero+1
                numero2=numero2+1

            host.legend( handles=plot_lns, loc='best' )
                #plt.plot( df.t , df[i] ,'.' , label=i)# worked good
            ###plt.plot( df.t , df.b, 'g-.d' )
            ###plt.plot( df.t , df.c, 'r-..' )
            plt.gcf().autofmt_xdate()   # rotates

            #print( "DDD .............. t     ", df.t )
            #print( "DDD .............. t[]   ", df.t[0] )  # last
            #print( "DDD .............. len   ", len(df.t) )  # last
            #print( "DDD .............. t[len] ", df.t[ len(df.t)-1 ] )  #
            #print( "DDD ..............       ",  (df.t[0] - df.t[ len(df.t)-1 ]).total_seconds()   )
            if  (df.t[0] - df.t[ len(df.t)-1 ]).total_seconds() <3600*24*3:  # Three days  3 days
                myFmt = mdates.DateFormatter('%y-%m-%d %H:%M') #
            else:
                myFmt = mdates.DateFormatter('%Y-%m-%d') #
                ####myFmt = mdates.DateFormatter('%H:%M') #
            plt.gca().xaxis.set_major_formatter(myFmt)
            #
            #plt.gca().grid() # move after host==ax1 before ax2
            #plt.legend( loc='upper left' )
            fig.tight_layout()
            #============================== PICTURE SHOULD BE OK NOW
            plt.text(0,1, datetime.datetime.now().strftime("%Y/%m/%d_%H:%M"),transform=plt.gca().transAxes, fontsize=8,   verticalalignment='bottom')


            if args.figure=="":
                plt.show()
            else:
                #fig.patch.set_facecolor( args.background )

                plt.savefig( args.figure  , bbox_inches='tight', facecolor=args.background  )
                #plt.savefig( args.figure , bbox_inches='tight' )


        if args.hdf5 != "":
            hdf = pd.HDFStore( args.hdf5 )
            name=DATABASE+"_"+COLLECTION+"_"+datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
            hdf.put( name  , df, format='table', data_columns=True)
            ks= hdf.keys()
            for i in ks:
                print( "  ", i, hdf[i].shape)
    ###################### WRITE SECTION #################
    else:
        if "t" in insdict:  # key in dict
            if DEBUG:print("D...  time insertion detected...")
            val=prepare_write_no_time( insdict )
        else:
            if DEBUG:print("D... automatic time...")
            val=prepare_write( insdict )
        result=collection.insert_one( val )
        print("result ID of write:", result.inserted_id )
